package br.com.rodolfoortale.spidersh;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import br.com.rodolfoortale.spidersh.asynctask.JSONCons;
import br.com.rodolfoortale.spidersh.asynctask.JSONObjectParser;
import br.com.rodolfoortale.spidersh.asynctask.find.FindMessages;
import br.com.rodolfoortale.spidersh.asynctask.send.MessageSended;
import br.com.rodolfoortale.spidersh.beans.Mensagem;
import br.com.rodolfoortale.spidersh.util.FormUtil;

/**
 * Created by USER on 11/06/15.
 */
public class MensagesActivity extends ListActivity {
    private String TAG = "MensagesActivity";

    final List<Mensagem> listItems = new CopyOnWriteArrayList<Mensagem>();

    ArrayAdapter<Mensagem> adapter;

    private Button btnEnviarTodas;

    private EditText edtTelefone;

    private String urlWS = JSONCons.URL_BR_WEBSERVICE;

    private ProgressDialog progressDialog;

    private Mensagem mensagem;
    private Mensagem mensagemEnviar;

    private TextView lbMinhasMensagens;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.mensagens_activity);

        progressDialog = new ProgressDialog(this);

        btnEnviarTodas = (Button) findViewById(R.id.btnEnviarTodas);

        lbMinhasMensagens = (TextView) findViewById(R.id.lbMinhasMensagens);

        edtTelefone = (EditText) findViewById(R.id.edtTelefone);
        edtTelefone.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        btnEnviarTodas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendAllMessages(edtTelefone.getText().toString());
            }
        });

        findMensagens();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void iniciaConsulta(String message) {
        if (!progressDialog.isShowing()) {
            progressDialog.setMessage(message);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }
    }

    private void finalizaConsulta() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    private void populateMensagens(JSONArray arrayMensagens) {
        try {
            for (int i = 0; i < arrayMensagens.length(); i++) {
                String strJson = arrayMensagens.get(i).toString();
                JSONObject jsonObject = new JSONObject(strJson);
                mensagem = new Mensagem();
                mensagem.setId(jsonObject.getInt(JSONCons.keyIdDados));
                mensagem.setMensagem(jsonObject.getString(JSONCons.keyMensagemMensagem));
                mensagem.setTelefone(jsonObject.getString(JSONCons.keyTelefoneMensagem));
                listItems.add(mensagem);
            }
        } catch (JSONException exc) {
            Log.e(TAG, "error: " + exc.getMessage());
        }

        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listItems);
        setListAdapter(adapter);
    }

    private void findMensagens() {
        Log.v(TAG, urlWS);

        FindMessages findMessages = new FindMessages() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                iniciaConsulta(getString(R.string.infoBuscandoMensagem));

                mensagem = new Mensagem();
            }

            @Override
            protected JSONObject doInBackground(JSONObject... params) {
                JSONObject json = null;
                try {
                    JSONObjectParser jsonObjectParser = new JSONObjectParser();
                    List<NameValuePair> paramsMensagens = new ArrayList<>();
                    paramsMensagens.add(new BasicNameValuePair(JSONCons.keyChave, mensagem.getChave()));
                    paramsMensagens.add(new BasicNameValuePair(JSONCons.keyController, JSONCons.createControllerMensagens));
                    paramsMensagens.add(new BasicNameValuePair(JSONCons.keyMetodo, JSONCons.getMethodMensagens));

                    json = jsonObjectParser.getJSONFromUrlPost(urlWS, paramsMensagens);

                    Log.v(TAG, json.toString());
                } catch (NullPointerException exc) {
                    Log.v(TAG, "erro: " + exc.getMessage());
                }

                return json;
            }

            @Override
            protected void onPostExecute(JSONObject mensagensConsultadas) {
                super.onPostExecute(mensagensConsultadas);

                try {
                    if (mensagensConsultadas != null) {
                        String totalMensagens = mensagensConsultadas.getString(JSONCons.keyTotalMensagem);
                        Boolean mensagensSuccess = mensagensConsultadas.getBoolean(JSONCons.keySuccess);
                        final JSONArray jsonArrayMensagens = mensagensConsultadas.getJSONArray(JSONCons.keyDados);

                        Log.v(TAG, "" + mensagensConsultadas.getString(JSONCons.keyTotalMensagem));
                        if (mensagensSuccess) {
                            if (jsonArrayMensagens.length() > 0)
                                lbMinhasMensagens.setText("Minhas Mensagens (" + totalMensagens + ")");

                            else {
                                Toast.makeText(MensagesActivity.this, "Você não possui mensagens", Toast.LENGTH_LONG).show();
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    populateMensagens(jsonArrayMensagens);
                                }
                            });

                            finalizaConsulta();
                        }

                        else {
                            finalizaConsulta();
                        }
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "error: " + e.getMessage());
                    e.printStackTrace();
                    finalizaConsulta();
                }
            }
        };
        findMessages.execute();
    }

    private void sendAllMessages(String telefone) {
        if (telefone.isEmpty() || !FormUtil.isTelephoneValid(edtTelefone.getText().toString())) {
            Toast.makeText(MensagesActivity.this, "Insira um telefone válido (Ex.: 999 9999-9999)", Toast.LENGTH_LONG).show();
        }
        else {
            Log.v(TAG, urlWS);

            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.titleEnviar))
                    .setMessage(getString(R.string.warningEnviarTodasMensagens))
                    .setPositiveButton(getString(R.string.labelSim), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            for (Mensagem auxMensagem : listItems) {
                                sendSMSMessage(edtTelefone.getText().toString(), auxMensagem);
                            }
                        }
                    })
                    .setNegativeButton(getString(R.string.labelNao), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    }).show();
        }
    }

    protected void sendSMSMessage(String phoneNumber, final Mensagem message) {
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";

        iniciaConsulta(getString(R.string.infoEnviandoMensagem));

        PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);

        this.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        setSended(message);
                        Toast.makeText(getBaseContext(), getString(R.string.infoMensagemEnviada), Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        Toast.makeText(getBaseContext(), getString(R.string.infoMensagemEnviada),
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        Toast.makeText(getBaseContext(), getString(R.string.errorSemSinal),
                                Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        Toast.makeText(getBaseContext(), getString(R.string.errorSemSinal),
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        Toast.makeText(getBaseContext(), getString(R.string.infoMensagemEntregue),
                                Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
                        Toast.makeText(getBaseContext(), getString(R.string.errorMensagemNaoEntregue),
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message.getMensagem(), sentPI, deliveredPI);
    }

    private void setSended(final Mensagem mensagemSended) {
        Log.v(TAG, urlWS);

        MessageSended messageSended = new MessageSended() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                mensagemEnviar = new Mensagem();
            }

            @Override
            protected JSONObject doInBackground(JSONObject... params) {
                JSONObject json = null;
                try {
                    JSONObjectParser jsonObjectParser = new JSONObjectParser();
                    List<NameValuePair> paramsMensagens = new ArrayList<>();
                    paramsMensagens.add(new BasicNameValuePair(JSONCons.keyChave, mensagemEnviar.getChave()));
                    paramsMensagens.add(new BasicNameValuePair(JSONCons.keyController, JSONCons.createControllerMensagens));
                    paramsMensagens.add(new BasicNameValuePair(JSONCons.keyMetodo, JSONCons.sendedMethodMensagens));
                    paramsMensagens.add(new BasicNameValuePair(JSONCons.keyIdMensagem, String.valueOf(mensagemSended.getId())));

                    json = jsonObjectParser.getJSONFromUrlPost(urlWS, paramsMensagens);

                    Log.v(TAG, json.toString());
                } catch (NullPointerException exc) {
                    Log.v(TAG, "error: " + exc.getMessage());
                }

                return json;
            }

            @Override
            protected void onPostExecute(JSONObject mensagem) {
                super.onPostExecute(mensagem);

                try {
                    if (mensagem != null) {
                        Boolean success = mensagem.getBoolean(JSONCons.keySuccess);
                        String mensagemMensagem = mensagem.getString(JSONCons.keyMensagemMensagem);
                        if (success) {
                            Toast.makeText(MensagesActivity.this, mensagemMensagem, Toast.LENGTH_LONG).show();
                            Log.v(TAG, "" + mensagem.getString(JSONCons.keyMensagemMensagem));

                            synchronized (listItems) {
                                Iterator<Mensagem> mensagemIterator = listItems.iterator();
                                while(mensagemIterator.hasNext()){
                                    Mensagem value = mensagemIterator.next();
                                    if (value.getId().equals(mensagemSended.getId())) {
                                        listItems.remove(mensagemSended);
                                        lbMinhasMensagens.setText("Minhas Mensagens (" + listItems.size() + ")");
                                        adapter.notifyDataSetChanged();
                                    }
                                }
                            }

                            if (listItems.size() == 0) {
                                Toast.makeText(MensagesActivity.this, "Você não possui mais mensagens", Toast.LENGTH_LONG).show();
                            }

                            finalizaConsulta();
                        }

                        else {
                            finalizaConsulta();
                            Toast.makeText(MensagesActivity.this, mensagemMensagem, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "error: " + e.getMessage());
                    e.printStackTrace();
                    finalizaConsulta();
                }
            }
        };
        messageSended.execute();
    }

    @Override
    protected void onListItemClick(ListView l, View v, final int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (edtTelefone.getText().toString().isEmpty() || !FormUtil.isTelephoneValid(edtTelefone.getText().toString())) {
            Toast.makeText(MensagesActivity.this, "Insira um telefone válido (Ex.: 999 9999-9999)", Toast.LENGTH_SHORT).show();
        }
        else {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.titleEnviar))
                    .setMessage(getString(R.string.warningEnviarMensgem))
                    .setPositiveButton(getString(R.string.labelSim), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Mensagem mensagemSelecionada = adapter.getItem(position);
                            sendSMSMessage(edtTelefone.getText().toString(), mensagemSelecionada);
                        }
                    })
                    .setNegativeButton(getString(R.string.labelNao), new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                        }
                    }).show();
        }
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.tittleSairDoAplicativo))
                .setMessage(getString(R.string.warningSairDoAplicativo))
                .setPositiveButton(getString(R.string.labelSim), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.labelNao), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                }).show();
    }
}