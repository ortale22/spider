package br.com.rodolfoortale.spidersh;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.rodolfoortale.spidersh.asynctask.JSONCons;
import br.com.rodolfoortale.spidersh.asynctask.JSONObjectParser;
import br.com.rodolfoortale.spidersh.asynctask.find.FindUsuario;
import br.com.rodolfoortale.spidersh.beans.Usuario;
import br.com.rodolfoortale.spidersh.util.FormUtil;

/**
 * Created by USER on 11/06/15.
 */
public class LoginActivity extends Activity {
    private String TAG = "LoginActivity";

    private Usuario usuarioConsultado;

    private EditText edtLogin;
    private EditText edtSenha;
    private Button btnLogin;
    private Button btnCancelar;
    private Button btnCadastrar;

    private ProgressDialog progressDialog;

    private String urlWS = JSONCons.URL_BR_WEBSERVICE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        progressDialog = new ProgressDialog(this);

        edtLogin     = (EditText) findViewById(R.id.edtLogin);
        edtSenha     = (EditText) findViewById(R.id.edtSenha);
        btnLogin     = (Button)   findViewById(R.id.btnLogin);
        btnCancelar  = (Button)   findViewById(R.id.btnCancelar);
        btnCadastrar = (Button)   findViewById(R.id.btnCadastrar);

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        edtLogin.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.KEYCODE_ENTER) {
                    validateLogin(edtLogin.getText().toString(), edtSenha.getText().toString());
                }

                return false;
            }
        });

        edtSenha.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.KEYCODE_ENTER) {
                    validateLogin(edtLogin.getText().toString(), edtSenha.getText().toString());
                }

                return false;
            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(LoginActivity.this)
                        .setTitle(getString(R.string.tittleSairDoAplicativo))
                        .setMessage(getString(R.string.warningSairDoAplicativo))
                        .setPositiveButton(getString(R.string.labelSim), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                finish();
                            }
                        })
                        .setNegativeButton(getString(R.string.labelNao), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                            }
                        }).show();
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateLogin(edtLogin.getText().toString(), edtSenha.getText().toString());
            }
        });

        btnCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, CreateUserActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.tittleSairDoAplicativo))
                .setMessage(getString(R.string.warningSairDoAplicativo))
                .setPositiveButton(getString(R.string.labelSim), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        finish();
                    }
                })
                .setNegativeButton(getString(R.string.labelNao), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {

                    }
                }).show();
    }

    private void iniciaConsulta(String message) {
        if (!progressDialog.isShowing()) {
            progressDialog.setMessage(message);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }
    }

    private void finalizaConsulta() {
        if (progressDialog.isShowing())
            progressDialog.dismiss();
    }

    private Boolean validateLogin(String login, String senha) {
        if (!FormUtil.isValidEmail(login) || !FormUtil.isEmailOrPasswordInRange(login)) {
            Toast.makeText(this, getString(R.string.errorUsuarioInvalido), Toast.LENGTH_SHORT).show();
        }
        else if(!FormUtil.isEmailOrPasswordInRange(senha)) {
            Toast.makeText(this, getString(R.string.errorSenhaInvalida), Toast.LENGTH_SHORT).show();
        }
        else {
            Log.v(TAG, urlWS);

            FindUsuario findUsuario = new FindUsuario() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    iniciaConsulta(getString(R.string.infoVerificandoUsuario));

                    usuarioConsultado = new Usuario();
                }

                @Override
                protected JSONObject doInBackground(JSONObject... params) {
                    JSONObject json = null;
                    try {
                        JSONObjectParser jsonObjectParser = new JSONObjectParser();
                        List<NameValuePair> paramsUserLogin = new ArrayList<>();
                        paramsUserLogin.add(new BasicNameValuePair(JSONCons.keyChave, usuarioConsultado.getChave()));
                        paramsUserLogin.add(new BasicNameValuePair(JSONCons.keyController, JSONCons.createControllerCadastro));
                        paramsUserLogin.add(new BasicNameValuePair(JSONCons.keyMetodo, JSONCons.loginMethodCadastro));
                        paramsUserLogin.add(new BasicNameValuePair(JSONCons.keyEmailDados, edtLogin.getText().toString()));
                        paramsUserLogin.add(new BasicNameValuePair(JSONCons.keySenhaDados, edtSenha.getText().toString()));

                        json = jsonObjectParser.getJSONFromUrlPost(urlWS, paramsUserLogin);

                        Log.v(TAG, json.toString());
                    } catch (NullPointerException exc) {
                        Log.v(TAG, "erro: " + exc.getMessage());
                    }

                    return json;
                }

                @Override
                protected void onPostExecute(JSONObject usuario) {
                    super.onPostExecute(usuario);

                    try {
                        if (usuario != null) {
                            Boolean success = usuario.getBoolean(JSONCons.keySuccess);
                            String mensagem = usuario.getString(JSONCons.keyMensagem);
                            if (success) {
                                JSONObject jsonObjectUser = usuario.getJSONObject(JSONCons.keyDados);
                                Log.v(TAG, "" + usuario.getJSONObject(JSONCons.keyDados).toString());

                                Integer idUsuario   = jsonObjectUser.getInt(JSONCons.keyIdDados);
                                String emailUsuario = jsonObjectUser.getString(JSONCons.keyEmailDados);

                                usuarioConsultado.setId(idUsuario);
                                usuarioConsultado.setEmail(emailUsuario);

                                finalizaConsulta();

                                Intent intent = new Intent(LoginActivity.this, MensagesActivity.class);
                                startActivity(intent);
                                finish();
                            }

                            else {
                                finalizaConsulta();
                                Toast.makeText(LoginActivity.this, mensagem, Toast.LENGTH_LONG).show();
                            }
                        }
                    } catch (JSONException e) {
                        Log.e(TAG, "error: " + e.getMessage());
                        e.printStackTrace();
                        finalizaConsulta();
                    }
                }
            };
            findUsuario.execute();
        }

        return false;
    }
}
