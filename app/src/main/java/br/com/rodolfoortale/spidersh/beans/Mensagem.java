package br.com.rodolfoortale.spidersh.beans;

/**
 * Created by USER on 11/06/15.
 */
public class Mensagem {
    private Integer id;
    private String telefone;
    private String mensagem;
    private String chave;

    public Mensagem() {
        chave = "AOFS2341OJAFS9815JKAF8";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getChave() {
        return chave;
    }

    @Override
    public String toString() {
        return mensagem.toString();
    }
}
