package br.com.rodolfoortale.spidersh.util;

/**
 * Created by USER on 12/06/15.
 */
public class FormUtil {
    public static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isEmailOrPasswordInRange(String target) {
        return target.length() >= 10 && target.length() <= 20;
    }

    public static boolean isTelephoneValid(String target) {
        return target.length() == 13;
    }
}
