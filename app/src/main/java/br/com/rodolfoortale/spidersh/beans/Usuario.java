package br.com.rodolfoortale.spidersh.beans;

/**
 * Created by USER on 11/06/15.
 */
public class Usuario {
    private Integer id;
    private String email;
    private String senha;
    private String chave;
    private Integer ativo;

    public Usuario() {
        chave = "AOFS2341OJAFS9815JKAF8";
        ativo = 1;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getChave() {
        return chave;
    }

    public Integer getAtivo() {
        return ativo;
    }
}
