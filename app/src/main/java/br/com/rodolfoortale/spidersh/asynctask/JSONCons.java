package br.com.rodolfoortale.spidersh.asynctask;

/**
 * Created by USER on 11/06/15.
 */
public class JSONCons {
    // URL
    public static final String URL_BR_WEBSERVICE = "http://2137.webmikrotik.com/ws/";

    /*
    * controller: cadastro
    */
    public static String createControllerCadastro = "cadastro";

    /*
    * controller: mensagens
    */
    public static String createControllerMensagens = "mensagens";

    /*
    * controller: cadastro
    * method: create
    */
    public static String createMethodCadastro = "create";

    /*
    * controller: cadastro
    * method: login
    */
    public static String loginMethodCadastro = "login";

    /*
    * controller: mensagens
    * method: get
    */
    public static String getMethodMensagens = "get";

    /*
    * controller: mensagens
    * method: sended
    */
    public static String sendedMethodMensagens = "sended";

    // Keys Values
    public static String keySuccess    = "success";
    public static String keyMensagem   = "mensagem";
    public static String keyChave      = "chave";
    public static String keyController = "controller";
    public static String keyMetodo     = "metodo";

    /*
    * controller: cadastro
    * method: login
    */
    public static String keyDados      = "dados";
    public static String keyIdDados    = "id";
    public static String keyEmailDados = "email";
    public static String keySenhaDados = "senha";
    public static String keyAtivoDados = "ativo";

    /*
    * controller: mensagens
    * method: create
    */
    public static String keyIdMensagem       = "id";
    public static String keyMensagemMensagem = "mensagem";
    public static String keyTelefoneMensagem = "telefone";

    /*
    * controller: mensagens
    * method: get
    */
    public static String keyTotalMensagem = "total";
}